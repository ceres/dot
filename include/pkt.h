#include<stdio.h>
#include<pcap.h>
#include<string.h>
#include<netinet/in.h>
#include<arpa/inet.h>

#define COUNT 1000
#define ETHER_SIZE 6

void capture(u_char *args, const struct pcap_pkthdr *hder, const u_char *packet);

struct ether_hd{
    u_char _dhost[ETHER_SIZE];
    u_char _shost[ETHER_SIZE];
    u_short ether_type;
};

struct ip_hd{
    u_char ipvhl;
    u_char tos;
    u_short ipl;
    u_short ident;
    u_short fl_frag;
    u_char ttl;
    u_char proto;
    u_short chksum;
    struct in_addr srcip,dstip;
};
struct tcp_hd{
    u_short sport;
    u_short dport;
    u_int seq;
    u_int ackn;
    u_short hdfl;
    u_short wsize;
    u_short cksum;
    u_short upntr;
};
