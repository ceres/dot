CFLAGS = -I include -L lib:/usr/lib/x86_64-linux-gnu
LDFLAGS = -lpcap
BUILD = build
LIB = lib 
VPATH = src include
#
all: build_msg ${BUILD}/pkt
#
#${BUILD}/mem: mem.c
#	${CC} ${CFLAGS} -o $@ $^
#${BUILD}/array: array.c
#	${CC} ${CFLAGS} -o $@ $^
#${BUILD}/funptr: funptr.c
#	${CC} ${CFLAGS} -o $@ $^
#${BUILD}/ll: ${LIB}/libds.so ll.c
#	${CC} ${CFLAGS} ${LDFLAGS} -o $@ $^
#${BUILD}/qq: ${LIB}/libds.so qq.c
#	${CC} ${CFLAGS} ${LDFLAGS} -o $@ $^
#${BUILD}/ss: ${LIB}/libds.so ss.c
#	${CC} ${CFLAGS} ${LDFLAGS} -o $@ $^
#
#${LIB}/libmast.so: mast.c
#	${CC} ${CFLAGS} -shared -fPIC -o ${LIB}/libmast.so $<
#${LIB}/libds.so: ds.c queue.c stack.c
#	${CC} ${CFLAGS} -shared -fPIC -o ${LIB}/libds.so $^
#	
${BUILD}/pkt: pkt.c
	${CC} ${CFLAGS} ${LDFLAGS} -o $@ $^
.PHONY: clean build_msg

clean:
	@rm -rf mem mem.o funptr funptr.o array array.o str str.o mast.o build lib

build_msg:
	@mkdir ${BUILD} ${LIB}
	@printf "\n# Build initiated by $(USER) \n\n"
