#include "pkt.h"

int main(int argc, char **argv){
    char *dev, errbuf[PCAP_ERRBUF_SIZE];
    dev = pcap_lookupdev(errbuf);
    //dev = "lo";
    pcap_t *handle;

    struct bpf_program fp;
    char filter_exp[] = "port 22";
    bpf_u_int32 mask;
    bpf_u_int32 net;

    struct pcap_pkthdr hdr;
    const u_char *pkt;


    if(dev == NULL){
        fprintf(stderr, "Couldn't find default device: %s\n",errbuf);
        return(1);
    }

    if(pcap_lookupnet(dev,&net,&mask,errbuf) == -1){
        fprintf(stderr,"Can't get netmask for device %s\n",dev);
        net = 0;
        mask = 0;
    }
    handle = pcap_open_live(dev,BUFSIZ,1,1000,errbuf);
    if(handle == NULL){
        fprintf(stderr, "Couldn't find default device: %s\n",errbuf);
        return(2);
    }
    if(pcap_datalink(handle) != DLT_EN10MB){
        fprintf(stderr, "Device doesn't support Ethernet headers\n",dev);
        return(6);
    }

    // Let's create some filters
    if(pcap_compile(handle,&fp,filter_exp,0,net) == -1){
        fprintf(stderr,"Couldn't parse filter %s: %s\n",filter_exp,pcap_geterr(handle));
        return(13);
    }
    if(pcap_setfilter(handle,&fp) == -1){
        fprintf(stderr,"Couldn't install filter %s: %s\n",filter_exp,pcap_geterr(handle));
        return(17);
    }

    //pkt = pcap_next(handle,&hdr);
    //printf("Header length [%d]\n",hdr.len);
    pcap_loop(handle,COUNT,capture,NULL);
    pcap_close(handle);

    //printf("Device: %s\n",dev);
    return(0);
}

void capture(u_char *args, const struct pcap_pkthdr *hder, const u_char *packet){
    struct ether_hd *ehd;
    struct ip_hd *iphd;
    struct tcp_hd *tcphd;
    ehd=(struct ether_hd*)packet;
    iphd=(struct ip_hd*)(packet+sizeof(struct ether_hd));
    tcphd=(struct tcp_hd*)(packet+sizeof(struct ether_hd)+sizeof(struct ip_hd));
    //printf("ipv: %x:hdrln: %x:tos: %x\n",((iphd->ipvhl)&0xf0)>>4,((iphd->ipvhl)&0x0f),iphd->tos,iphd->ttl);
    //printf("ipv: %x:hdrln: %x:tos: %x:ttl: %x\n",((iphd->ipvhl)&0xf0)>>4,((iphd->ipvhl)&0x0f),iphd->tos,iphd->ttl);
    printf("HEADERs\n");
    printf("ipv: %x:hdrln: %02x:tos: %x:pktln: %04x\n",(((iphd->ipvhl)&0xf0)>>4),((iphd->ipvhl)&0x0f),iphd->tos,ntohs(iphd->ipl));
    printf("ident: %04x:flags: %x:fragoff: %03x\n",ntohs(iphd->ident),((ntohs(iphd->fl_frag)&0xd000)>>12),(ntohs((iphd->fl_frag))&0x3fff));
    printf("ttl: %0x:proto: %0x:checksum: %04x\n",iphd->ttl,iphd->proto,ntohs(iphd->chksum));
    printf("src: %s ",inet_ntoa(iphd->srcip));
    printf("dst: %s\n",inet_ntoa(iphd->dstip));
    printf("sport: %04x:dport: %04x\n",ntohs(tcphd->sport),ntohs(tcphd->dport));
    printf("seq: %08x: ackno: %08x\n",ntohl(tcphd->seq),ntohl(tcphd->ackn));
    printf("hdl: %x: flags: %x wsize: %x\n",((ntohs(tcphd->hdfl) & 0xf000)>>12),(ntohs(tcphd->hdfl)& 0x003f),ntohs(tcphd->wsize));
    printf("cksum: %04x: upointer: %04x\n",ntohs(tcphd->cksum),ntohs(tcphd->upntr));
    printf("\n\n");
}
